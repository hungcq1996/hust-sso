package edu.hust.sso;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SsoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SsoApplicationTests {

    @Test
    public void whenLoadApplication_thenSuccess() {

    }

}
