package edu.hust.sso.auth;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HustUserResponse {
    private int status;
//    private HustUser data;
    private String data;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
    
    
}
