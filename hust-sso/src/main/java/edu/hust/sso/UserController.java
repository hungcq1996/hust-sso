package edu.hust.sso;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hust.sso.auth.HustUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @RequestMapping("/user/me")
    public Principal user(Principal principal) {
        System.out.println(principal.toString());
        return principal;
    }
}
